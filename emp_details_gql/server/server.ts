const express = require('express');
import { graphqlHTTP } from 'express-graphql';
import { buildSchema } from 'graphql';
const mysql = require('mysql');
const cors = require('cors');

const app = express();
app.use(cors());

// create connection with mysql database
const db = mysql.createConnection({
    host: 'localhost',
    user: 'Hrishikesh',
    password: '123',
    database: 'employee_details'
  });
  
  db.connect((err) => {
    if(err) throw err;
    console.log('Connected to Mysql Server');
  });
  
  
  
  var schema = buildSchema(`
      type employee {
        employee_id: Int
        employee_name: String
        employee_age: Int
        employee_salary: Int
        active_status: Int
      }
      type Query {
        getEmployee: [employee],
        getEmployeeInfo(employee_id: Int!) : employee
      }
      type Mutation {
        updateEmpInfo(employee_id: Int!, employee_name: String!, employee_age: Int!, employee_salary: Int!, active_status: Int!): Boolean
        createEmp(employee_id: Int!, employee_name: String!, employee_age: Int!, employee_salary: Int!, active_status: Int!): Boolean
        deleteEmp(employee_id: Int!): Boolean
      }
  `);

  
  const queryDB = (req, sql, args) => new Promise((resolve, reject) => {
    db.query(sql, args, (err, rows) => {
        if (err)
            return reject(err);
        rows.changedRows || rows.affectedRows || rows.insertId ? resolve(true) : resolve(rows);
    });
  });
  
  var root = {
    getEmployee: (args, req) => queryDB(req, "select * from employee", null).then(data => data),
    getEmployeeInfo: (args, req) => queryDB(req, "select * from employee where employee_id = ?", [args.employee_id]).then(data => data[0]),
    createEmp: (args, req) => queryDB(req, "insert into employee SET ?", args).then(data => data),
    updateEmpInfo: (args, req) => queryDB(req, "update employee SET ? where employee_id = ?", [args, args.employee_id]).then(data => data),
    deleteEmp: (args, req) => queryDB(req, "delete from employee where employee_id = ?", [args.employee_id]).then(data => data)
  };
  
  
  app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue:root,
    graphiql: true,
  }));

  
    
  
  
  app.listen(4000);
  
  console.log('Running a GraphQL API server at localhost:4000/graphql');