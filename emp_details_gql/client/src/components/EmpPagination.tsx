import React, { Component, useContext } from "react";
import { employeeContext } from "../employeeContext";
import { useState } from "react";
import { useEffect } from "react";

export const EmpPagination = ({
  itemsPerPage,
  onPaginationChange,
  totalRecords,
}: any) => {
  const [counter, setCounter] = useState(1);
  const totalPages = Math.ceil(totalRecords / itemsPerPage);

  useEffect(() => {
    const value = itemsPerPage * counter;
    onPaginationChange(value - itemsPerPage, value);
  }, [counter]);

  const changePage = (type: string) => {
    if (type === "prev") {
      if (counter === 1) {
        setCounter(1);
      } else {
        setCounter(counter - 1);
      }
    } else if (type === "next") {
      if (Math.ceil(totalRecords / itemsPerPage) === counter) {
        setCounter(counter);
      } else {
        setCounter(counter + 1);
      }
    }
  };
  return (
    <div>
      <button
        id="paginationButton"
        className="btn btn-xs"
        onClick={() => changePage("prev")}
      >
        Previous
      </button>
      {new Array(totalPages).fill(" ").map((ele: any, index: number) => (
        <button
          id="paginationButton"
          key={index + 1}
          onClick={() => setCounter(index + 1)}
          className={`page-item ${index + 1 === counter ? "btn active" : null}`}
        >
          {index + 1}
        </button>
      ))}
      <button
        id="paginationButton"
        className="btn btn-secondary btn-xs"
        onClick={() => changePage("next")}
      >
        &nbsp;Next&nbsp;&nbsp;
      </button>
    </div>
  );
};
