import React from "react";
import { useState } from "react";
import { UserTable } from "./UserTable";
import { employeeContext } from "../employeeContext";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_EMPLOYEES, GET_SPECIFIC_EMP, ADD_EMPLOYEE } from "../queries";

var canInsert = 0;
export function Form() {
  const [showEmployees, setShowEmployees] = useState(true);
  const [empId, setEmpId] = useState("");
  const [empName, setEmpName] = useState("");
  const [empAge, setEmpAge] = useState("");
  const [empSalary, setEmpSalary] = useState("");
  const [successfulSubmission, setSuccessfulSubmission] = useState(false);

  const getAllEmployees = useQuery(GET_EMPLOYEES);
  const getSpecificEmp = useQuery(GET_SPECIFIC_EMP, {
    variables: { employee_id: 1 },
  });

  const [createEmp] = useMutation(ADD_EMPLOYEE);

  if (getAllEmployees.loading || getSpecificEmp.loading)
    return <h2>Loading...</h2>;
  else {
    console.log(getAllEmployees.data);
    console.log(getSpecificEmp.data);
  }

  return (
    <employeeContext.Provider value={[getAllEmployees.data]}>
      <div className="form">
        <h2>Add Employee!</h2>
        {successfulSubmission && <h2 id="empAdd">Employee Added!</h2>}
        <label>Employee Id</label>
        <input
          className="form-control"
          type="text"
          name="empId"
          value={empId}
          onChange={(e) => setEmpId(e.target.value)}
        />
        <label>Employee Name</label>
        <input
          className="form-control"
          type="text"
          name="empName"
          value={empName}
          onChange={(e) => setEmpName(e.target.value)}
        />
        <label>Age</label>
        <input
          className="form-control"
          type="text"
          name="empAge"
          value={empAge}
          onChange={(e) => setEmpAge(e.target.value)}
        />
        <label>Salary</label>
        <input
          className="form-control"
          type="text"
          name="empSalary"
          value={empSalary}
          onChange={(e) => setEmpSalary(e.target.value)}
        />
        <button
          id="submit-btn"
          className="btn btn-primary"
          onClick={(e) => {
            e.preventDefault();
            createEmp({
              variables: {
                employee_id: Number(empId),
                employee_name: empName,
                employee_age: Number(empAge),
                employee_salary: Number(empSalary),
                active_status: 1,
              },
            });
          }}
        >
          Submit
        </button>
        <br></br>

        {showEmployees && <UserTable />}

        <div id="root"></div>
      </div>
    </employeeContext.Provider>
  );
}
