import React from "react";
import { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_SPECIFIC_EMP } from "../queries";

export const Search = () => {
  const [searchEmp, setSearchEmp] = useState("");
  const [empNotFound, setEmpNotFound] = useState(false);
  const numSearchEmp = Number(searchEmp);

  const getSpecificEmp = useQuery(GET_SPECIFIC_EMP, {
    variables: { employee_id: numSearchEmp },
  });

  if (getSpecificEmp.loading) return <h2>Loading...</h2>;
  else {
    console.log(getSpecificEmp.data);
  }

  const searchForEmployee = (searchEmp: any) => {
    setEmpNotFound(false);
    console.log(getSpecificEmp.data);
  };
  return (
    <>
      <tr>
        <td>
          <input
            id="searchText"
            type="text"
            className="form-control"
            placeholder="search by Emp Id"
            value={searchEmp}
            onChange={(e) => setSearchEmp(e.target.value)}
          />
        </td>
        <td>
          <button
            className="btn btn-success btn-xs"
            onClick={() => searchForEmployee(searchEmp)}
          >
            Search
          </button>
        </td>
      </tr>

      <tr>
        <td>
          {empNotFound && <p style={{ color: "red" }}>Employee Not Found!</p>}
        </td>
      </tr>
      <hr></hr>
      <pre>{!empNotFound && JSON.stringify(getSpecificEmp.data, null, 2)}</pre>
    </>
  );
};
