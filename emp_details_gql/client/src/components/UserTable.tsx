import React, {useState, useContext} from 'react';
import { UserRow } from './UserRow';
import { Search } from './Search';
import { EmpPagination }  from './EmpPagination';
import { employeeContext } from '../employeeContext';

export const UserTable = () => {
 
    const [getAllEmployees] = useContext(employeeContext);
    const [successfulUpdation, setSuccessfulUpdation] = useState(false);
    const [ itemsPerPage, setItemsPerPage ] = useState(6);
    const [ pagination, setPagination ] = useState({
        start: 0,
        end: itemsPerPage
    });
    const empData: any = getAllEmployees;

    const onPaginationChange = (start: number, end: number) => {
        setPagination({start: start, end: end});
    }

    const handleSort = (type: string) => {
        sortRecords([...empData.getEmployee], type);
    }

    const sortRecords = (empData: any[], type: string) => {
        /*Axios.get(`http://localhost:3001/api/sort/${type}/${pagination.start}/${pagination.end}`).then((response) => {
            console.log(response.data)
            setEmployeeDetails(response.data);
        })*/
        let sortedEmp = [];
        if(type === 'employee_name'){
            sortedEmp = empData.sort((a: any, b: any) => {
                return a.employee_name > b.employee_name ? 0 : -1;
            });
        }else{
            sortedEmp = empData.sort((a: any, b: any) => {
                return a.employee_id < b.employee_id ? 0 : -1;
            });
        }
        
        console.log(sortedEmp)
    }

    return(
        <>
            <div className="empTable">
                <Search/>
                {successfulUpdation &&  <h2 id="empUpdate">Employee Updated!</h2>}
                <tr>
                    <th onClick={() => handleSort('employee_id')}>Emp ID |&nbsp;</th>
                    <th onClick={() => handleSort('employee_name')}>Employee Name |&nbsp;</th>
                    <th>Emp Age |&nbsp;</th>
                    <th>Emp Salary |&nbsp;</th>
                    <th>Emp Active?</th>
                </tr>
                {empData.getEmployee.slice(pagination.start, pagination.end).map((emp: Object, index: number) => <UserRow key={index+1} emp={emp}/>)}

            </div>
            <div id="marginPagination">
                <EmpPagination itemsPerPage={itemsPerPage} onPaginationChange={onPaginationChange} totalRecords={empData.getEmployee.length}/>
            </div>
        </>
    )
}