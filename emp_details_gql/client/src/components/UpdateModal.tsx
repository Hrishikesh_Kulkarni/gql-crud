import React, { useState } from "react";
import Modal from "react-modal";
import { useMutation } from "@apollo/react-hooks";
import { EDIT_EMPLOYEE } from "../queries";

Modal.setAppElement("#root");

export const UpdateModal = ({ emp, modalIsOpen, setModalIsOpen }: any) => {
  const [newEmpName, setNewEmpName] = useState("");
  const [newEmpAge, setNewEmpAge] = useState("");
  const [updateEmpInfo] = useMutation(EDIT_EMPLOYEE);

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      height: "50%",
      width: "40%",
    },
  };

  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={() => setModalIsOpen(false)}
      style={customStyles}
    >
      <h2>Update Employee Details</h2>
      <div className="updateForm">
        <label>Employee Name</label>
        <input
          type="text"
          className="form-control"
          onChange={(e) => setNewEmpName(e.target.value)}
        />
        <label>Employee Age</label>
        <input
          type="text"
          className="form-control"
          onChange={(e) => setNewEmpAge(e.target.value)}
        />
      </div>
      <br></br>
      <button
        className="btn btn-primary"
        onClick={() => {
          updateEmpInfo({
            variables: {
              employee_id: emp.employee_id,
              employee_name: newEmpName,
              employee_age: Number(newEmpAge),
              employee_salary: emp.employee_salary,
              active_status: 1,
            },
          });
          setModalIsOpen(false);
        }}
      >
        Update
      </button>
      &nbsp;
      <button className="btn btn-danger" onClick={() => setModalIsOpen(false)}>
        Close
      </button>
    </Modal>
  );
};
