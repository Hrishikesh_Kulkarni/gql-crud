import gql from 'graphql-tag';


export const GET_EMPLOYEES = gql`
    {
        getEmployee {
            employee_id
            employee_name
            employee_age
            employee_salary
            active_status
        }
    }
`;    

export const GET_SPECIFIC_EMP = gql`
  query ($employee_id: Int!){
    getEmployeeInfo(employee_id: $employee_id) {
      employee_id,
      employee_name,
      employee_age,
      employee_salary,
      active_status
    }
  }
`;

export const ADD_EMPLOYEE = gql`
  mutation($employee_id: Int!, $employee_name: String!, $employee_age: Int!, $employee_salary:Int!, $active_status: Int!) {
    createEmp (employee_id: $employee_id, employee_name: $employee_name, employee_age: $employee_age, employee_salary: $employee_salary, active_status: $active_status)
  }
`;


export const DELETE_EMPLOYEE = gql`
  mutation($employee_id: Int!) {
    deleteEmp(employee_id: $employee_id)
  }
`

export const EDIT_EMPLOYEE = gql`
mutation($employee_id: Int!, $employee_name: String!, $employee_age: Int!, $employee_salary: Int!, $active_status: Int!) {
    updateEmpInfo (employee_id: $employee_id, employee_name: $employee_name, employee_age: $employee_age, employee_salary: $employee_salary, active_status: $active_status)
  }
`;
