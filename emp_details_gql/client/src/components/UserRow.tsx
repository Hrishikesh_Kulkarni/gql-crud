import React, {useState, useContext} from 'react';
import Axios from 'axios';
import { UpdateModal } from './UpdateModal';
import { employeeContext } from '../employeeContext';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_EMPLOYEES, GET_SPECIFIC_EMP, ADD_EMPLOYEE, DELETE_EMPLOYEE, EDIT_EMPLOYEE } from '../queries';


export const UserRow = ({emp}: any) => {
    const [ employeeDetails, setEmployeeDetails ] = useContext(employeeContext);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [successfulDeletion, setSuccessfulDeletion] = useState(false);
    const [deleteEmp] = useMutation(DELETE_EMPLOYEE);

    /*const deleteEmp = (delEmp_Id) => {
        /*Axios.delete(`http://localhost:3001/api/delete/${delEmp_Id}`);
        setTimeout(() => {
            Axios.get("http://localhost:3001/api/get").then((response) => {
                console.log(response.data)
                setEmployeeDetails(response.data);
            });
        }, 1000);

        setSuccessfulDeletion(true);
    }*/

    const openModal = () => {
        setModalIsOpen(true);
    }
    return (
        <tr>
            <td>{emp.employee_id}</td>
            <td>{emp.employee_name}</td>
            <td>{emp.employee_age}</td>
            <td>{emp.employee_salary}</td>
            <td>{emp.active_status}</td>
            <button id="editDel-btn" className="btn btn-warning btn-xs" onClick={() => openModal()}>Edit</button>
            &nbsp;<button id="editDel-btn" className="btn btn-danger btn-xs" onClick={() => deleteEmp({ variables: { employee_id: emp.employee_id } })}>Delete</button>
            <td>{successfulDeletion &&  <p id="empDel">Deleted</p>}</td> 
            {modalIsOpen && <UpdateModal emp={emp} modalIsOpen={modalIsOpen} setModalIsOpen={setModalIsOpen}/>}  
        </tr>
    )
}